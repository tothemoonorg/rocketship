from enum import Enum


class OrderTypes(Enum):
    MARKET = 'market'
    LIMIT = 'limit'
    STOP = 'stop'


class OrderDuration(Enum):
    DAY = 'day'
    GOOD_UNTIL_CANCELLED = 'gtc'
    GOOD_UNTIL_DATE = 'gtd'


class OrderCondition(Enum):
    NONE = ''
    IMMEDIATE_OR_CANCEL = 'ioc'
    ALL_OR_NONE = 'aon'  # Also known as FILL_OR_KILL
