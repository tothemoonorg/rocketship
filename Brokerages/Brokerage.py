from abc import ABC, abstractmethod
from .Order import OrderTypes, OrderDuration, OrderCondition

class Brokerage(ABC):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def authenticate(self):
        pass

    ###################
    #   Profile
    ###################

    @abstractmethod
    def num_day_trades(self):
        pass

    @abstractmethod
    def stock_positions(self):
        pass

    @abstractmethod
    def stock_history(self):
        pass

    ###################
    #   Trade
    ###################

    @abstractmethod
    def order(self,
              quantity,
              order_type=OrderTypes.MARKET,
              order_duration=OrderDuration.GOOD_UNTIL_CANCELLED,
              order_condition=OrderCondition.NONE):
        pass
