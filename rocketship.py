from Brokerages.Robinhood.Robinhood import Robinhood
from TrailingStopLoss import TrailingStopLoss

import pandas_market_calendars as mcal
import pandas as pd
import threading
import configparser

class Rocketship():

    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')

        self.setup_trader(**config['AUTHENTICATION'])
        self.algorithm = TrailingStopLoss(self.trader)
        self.nyse = mcal.get_calendar('NYSE')

    def setup_trader(self, username, password):
        self.trader = Robinhood()
        self.trader.login(username=username, password=password)
    
    def run(self):
        threading.Timer(1, self.run).start() # Run every second

        now = pd.Timestamp.now(self.nyse.tz.zone)

        trading_day = self.nyse.schedule(start_date=now, end_date=now)

        if self.nyse.open_at_time(trading_day, now):
            self.algorithm.run(now)

if __name__ == "__main__":
    rocketship = Rocketship()
    rocketship.run()
