import requests

def get_current_price(instrument):
        quote = requests.get(instrument).json()['quote']
        return float(requests.get(quote).json()['ask_price'])

class TrailingStopLoss():

    max_prices = {}

    def __init__(self, trader, stop_loss=0.03):
        self.stop_loss = 1 - stop_loss
        self.trader = trader

    def run(self, now):
        positions = { x['instrument']: float(x['quantity']) for x in self.trader.positions()['results'] if float(x['quantity']) > 0 }
        self.clean_max_prices(positions)

        current_prices = { p: get_current_price(p) for p in positions }
        self.update_max_prices(current_prices)
        self.sell_stop_loss(positions, current_prices)

        print(self.max_prices)

    def clean_max_prices(self, positions):
        for i in self.max_prices.keys():
            if i not in positions:
                del self.max_prices[i]

    def update_max_prices(self, current_prices):
        for i, p in current_prices.items():
            if i not in self.max_prices:
                self.max_prices[i] = p
            self.max_prices[i] = max(p, self.max_prices[i])

    def sell_stop_loss(self, positions, current_prices):
        for i, p in current_prices.items():
            if p < (self.max_prices[i] * self.stop_loss):
                symbol = requests.get(i).json()['symbol']
                price = round(self.max_prices[i] * self.stop_loss, 2)
                self.trader.make_stop_loss_order(
                    instrument=i,
                    symbol=symbol,
                    price=price,
                    quantity=positions[i])
                print("Sold {} of {}".format(positions[i], symbol))