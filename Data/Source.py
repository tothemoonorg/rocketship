from abc import ABC, abstractmethod


class Source(ABC):

    def __init__(self):
        super().__init__()

    @abstractmethod
    def authenticate(self):
        pass
